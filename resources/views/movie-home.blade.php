@extends('layouts/master')

@section('judul')
Halaman Utama
@endsection

@section('content')
    <h1>SanberBook</h1>
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    <br>
    <h3>Tampilan Tabel Cast</h3>
    <ol>
        <li>Untuk melihat data tabel tekan <a href="/cast">Link ini</a></li>
        <li>Untuk tambah data tabel tekan <a href="/cast/create">Link ini</a></li>
    </ol>
@endsection
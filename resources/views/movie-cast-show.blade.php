@extends('layouts/master')

@section('judul')
Tampilan Detail Cast
@endsection

@section('content')
    <h1>Data dari {{$castShow->nama}} <br></h1>
    <h4>Nama : {{$castShow->nama}}</h4>
    <h4>Umur : {{$castShow->umur}}</h4>
    <h4>Bio : {{$castShow->bio}}</h4>
    <a href="/cast" type="button" class="btn btn-secondary">Kembali</a>
@endsection
@extends('layouts/master')

@section('judul')
Tabel Data Cast
@endsection

@push('script')
  <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush

@push('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Data</a>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Id Cast</th>
              <th>Nama</th>
              <th>Umur</th>
              <th>Bio</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->umur}}</td>
                        <td>{{$item->bio}}</td>
                        <td>
                        <form action="/cast/{{$item->id_cast}}" method="POST">
                            <a href="/cast/{{$item->id_cast}}/edit" type="button" class="btn btn-success">Update</a>
                            <a href="/cast/{{$item->id_cast}}" type="button" class="btn btn-info">Detail</a>
                            @csrf
                            @method('delete')
                            <input type="submit" value="Delete" class="btn btn-danger" onclick="return confirm('Apakah anda yakin ingin hapus data')">
                        </form>
                    </td>
                    </tr>
                @empty
                <h1>Data Cast Kosong</h1>
                @endforelse
            </tbody>
        </table>
@endsection
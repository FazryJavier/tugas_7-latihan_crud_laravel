@extends('layouts/master')

@section('judul')
Edit Data Cast
@endsection

@section('content')
<form method="POST" action="{{url('/cast/'.$castEdit->id_cast)}}">
    @csrf
    @method('PUT')
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" value="{{$castEdit->nama}}" name="nama" class="form-control">
        </div> 
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="mb-3">
            <label for="umur" class="form-label">Umur</label>
            <input type="text" value="{{$castEdit->umur}}" name="umur" class="form-control">
        </div> 
        @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="mb-3">
            <label for="bio" class="form-label">Bio</label>
            <textarea name="bio"  class="form-control" rows="3">{{$castEdit->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <a href="/cast" type="button" class="btn btn-secondary">Kembali</a>
        <button type="submit" class="btn btn-primary  ml-3">Update Data</button>
    </form>  
@endsection